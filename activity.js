// Find users with letter s in their first name or d in their last name with $or and case insesitive, display only first Name and last Name and hide id.

db.users.find({$or: [{ firstName: {$regex: 's', $options: '$i' } }, { lastName: {$regex: 'd', $options: '$i' } }]}, {

    firstName: 1,
    lastName: 1,
    _id: 0
})

// find age greater than or equal to 70 and from HR department
db.users.find({ $and: [ {department: 'HR'},{age: { $gte: 70 } }]})

// find age that is less than or equal to 30 and with an e on their first name

db.users.find({ $and: [ { firstName: {$regex: 'e', $options: '$s' } },{age: { $lte: 30 } }]})